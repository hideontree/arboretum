package com.yh.gui;

import javax.swing.*;

public class GameFrame extends JFrame {
    public GameFrame() {
        setTitle("Arboretum");
        setSize(1200, 700);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
    }
}
